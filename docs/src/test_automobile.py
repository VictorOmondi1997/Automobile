import unittest 
from automobile import Automobile


class TestAutomobile(unittest.TestCase):
	def testID(self):
		auto = Automobile(123,'sedan','merceedes','red',1994,2000)
		self.assertEqual(auto.vehicle_id, 123)
		
	def testModel(self):
		auto = Automobile(123,'sedan','merceedes','red',1994,2000)
		self.assertEqual(auto.get_model(), 'merceedes')


if __name__ == '__main__': 
	unittest.main()