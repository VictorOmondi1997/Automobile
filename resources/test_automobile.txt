import unittest 
from automobile import Automobile


class TestAutomobile(unittest.TestCase):
	def testID(self):
		auto = Automobile(123,'sedan','merceedes','red',1994,2000)
		self.assertEqual(self.auto.vehicle_id, 123)
		
	def testModel(self):
		auto = Automobile(123,'sedan','merceedes','red',1994,2000)
		self.assertEqual(self.auto.get_model(), 'sedan')


if __name__ == '__main__': 
	unittest.main()